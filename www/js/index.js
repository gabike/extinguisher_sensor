var button = {
service: "FFE0",
data: "FFE1", // Bit 2: side key, Bit 1- right key, Bit 0 –left key
};

var battery = {
	service: "180F",
	data: "2A19"
};

var accelerometer = {
service: "F000AA80-0451-4000-B000-000000000000",
data: "F000AA81-0451-4000-B000-000000000000", // read/notify 3 bytes X : Y : Z
notification:"F0002902-0451-4000-B000-000000000000",
configuration: "F000AA82-0451-4000-B000-000000000000", // read/write 1 byte
period: "F000AA83-0451-4000-B000-000000000000" // read/write 1 byte Period = [Input*10]ms
};    

var luxometer = {
service: 'f000aa70-0451-4000-b000-000000000000',
data: 'f000aa71-0451-4000-b000-000000000000',
configuration: 'f000aa72-0451-4000-b000-000000000000',
period: 'f000aa83-0451-4000-b000-000000000000',
};
var LEFT_BUTTON = 1;  // 0001
var RIGHT_BUTTON = 2; // 0010
var REED_SWITCH = 4;  // 0100

var connectedDeviceID = 0;
var searchingForDevices = false;
var listItem;
var isReconnectInProgress = false;
var reconnectInt;

var SensorTag = function (deviceId) {
	this.deviceId = deviceId;
	this.isdeviceConnectd = false;
	this.scheduledInt;
	this.reconnectInt;
	this.isBattCriticSms;
	this.batteryLevel = 0;
	this.batteryLevelPrev = 0;
	this.batterySampleCount = 0;
	this.isFirstBattSample = true;
	this.accelCounter = 0;
	this.xf = 0;
	this.yf = 0;
	this.zf = 0;
	this.pitch = 0;
	this.gpsCounter = 601;

	console.log("construct" + this.deviceId);
};

SensorTag.prototype.connect = function () {
	console.log("On Connect" + this.deviceId);
	this.isBattCriticSms = false;
	ble.connect(this.deviceId, this.onConnect.bind(this), app.onError);
};

SensorTag.prototype.onConnect = function (event) {
	console.log("On OnConnect");
//	hideModal();
	bindThing(this.deviceId);
	addBindedName(this.deviceId);
	this.buttonService();
	this.accelerometerService();
	this.scheduledInfo();
	// keep calling the readRssi() so we know multiple connections work
	this.scheduledInt = setInterval(this.scheduledInfo.bind(this), 10000);
};

SensorTag.prototype.scheduledInfo = function () {
	//Check if connected
	ble.isConnected(this.deviceId, this.onIsConnected.bind(this), this.onIsDisconnected.bind(this));
};

SensorTag.prototype.buttonService = function () {
	//Subscribe to button service
	ble.startNotification(this.deviceId, button.service, button.data, this.onButtonData.bind(this), app.onError);
	console.log("Button notification subscription SUCCESS ");
};

SensorTag.prototype.accelerometerService = function () {
	//Subscribe to accelerometer service
	ble.startNotification(this.deviceId, accelerometer.service, accelerometer.data, this.onAccelerometerData.bind(this), app.onError);

	// turn accelerometer on
	var configData = new Uint16Array(1);
	//Turn on gyro, accel, and mag, 2G range, Disable wake on motion
	configData[0] = 0x007F;
	ble.write(this.deviceId, accelerometer.service, accelerometer.configuration, configData.buffer,
			  function () { console.log("Started accelerometer."); }, app.onError);

	var periodData = new Uint8Array(1);
	periodData[0] = 0x0A;
	ble.write(this.deviceId, accelerometer.service, accelerometer.period, periodData.buffer,
			  function () { console.log("Configured accelerometer period."); }, app.onError);

	console.log("Accelerometer notification subscription SUCCESS ");
};

SensorTag.prototype.onButtonData = function (data) {
	var state = new Uint8Array(data);
	var message = '';

	if (state[0] == 0) {
		button1val = 0;
		button2val = 0;
	}

	if (state[0] == 1) {
		button1val = 1;
		button2val = 0;
		var finalcolor = "hsla(180,100%,50%,.7)";
		$('#cube figure').css('background', finalcolor);
	}

	if (state[0] == 2) {
		button1val = 0;
		button2val = 1;
		var finalcolor = "hsla(120,100%,50%,.7)";
		$('#cube figure').css('background', finalcolor);
	}
	console.log("BUTTON pressed on " + this.deviceId);
	buttonState.innerHTML = message;
	publishButton(this.deviceId);
};

SensorTag.prototype.onBatteryData = function (data) {
	var state = new Uint8Array(data);

	console.log("onBatteryData " + state[0] + "   " + this.batteryLevelPrev + "   " + this.batteryLevel + "   " + this.batterySampleCount);

	if (this.isFirstBattSample) {
		this.batteryLevel = state[0];
		publishBattery(this.deviceId, this.batteryLevel);
		this.isFirstBattSample = false;
		this.batteryLevelPrev = state[0];
	}
	else {
		if (state[0] != this.batteryLevelPrev) {
			console.log("onBatteryData 1");
			this.batteryLevelPrev = state[0];
			this.batterySampleCount = 0;
		}
		else
			this.batterySampleCount++;


		console.log("BATTERY level is " + this.batteryLevel + "%");
		if (this.batterySampleCount == 3) {
			console.log("onBatteryData 2");
			this.batteryLevel = state[0];
			this.batterySampleCount = 0;
			if (this.isBattCriticSms == false && this.batteryLevel <= 65) {
				this.isBattCriticSms = true;
/* 				if (this.deviceId == "A0:E6:F8:AE:CA:04")
					app.sendSms("WARNING - Battery level Critical for Fire Extinguisher Ext 02: " + this.batteryLevel + "%");
				else if (this.deviceId == "B0:B4:48:D2:33:06")
					app.sendSms("WARNING - Battery level Critical for Fire Extinguisher Ext 01: " + this.batteryLevel + "%");
 */		}
		publishBattery(this.deviceId, this.batteryLevel);
		}
	}
};

SensorTag.prototype.onAccelerometerData = function (data) {
	var message;
	var a = new Int16Array(data);

	// Update orientation of box

	var alpha = 0;
	//Low Pass Filter
	this.xf = this.xf * alpha + (app.sensorMpu9250AccConvert(a[3]) * (1.0 - alpha));
	this.yf = this.yf * alpha + (app.sensorMpu9250AccConvert(a[4]) * (1.0 - alpha));
	this.zf = this.zf * alpha + (app.sensorMpu9250AccConvert(a[5]) * (1.0 - alpha));

	//Roll & Pitch Equations
	var beta = 0.90;

	//calculate yaw rotation based on gyro and normalize [-180,180]
	var rotationSinceLast = app.sensorMpu9250GyroConvert(a[0]) * -0.1;
	if (((Math.atan2(-this.yf, this.zf) * 180.0) / 3.14) < 0) {
		if (yaw - 180 > 60) yaw = yaw - 360;
	} else {
		if (yaw - 180 < -60) yaw = yaw + 360;
	}

	yaw = ((Math.atan2(-this.yf, this.zf) * 180.0) / 3.14 + 180) * (1.0 - beta) +
    (rotationSinceLast + yaw) * beta;
	this.pitch = ((Math.atan2(this.xf, Math.sqrt(this.yf * this.yf + this.zf * this.zf)) * 180.0) / 3.14) * (1.0 - beta) +
    (app.sensorMpu9250GyroConvert(a[1]) * 0.1 + this.pitch) * beta;

	zAccel = app.sensorMpu9250AccConvert(a[5]);
	yAccel = app.sensorMpu9250AccConvert(a[4]);
	xAccel = app.sensorMpu9250AccConvert(a[3]);
	zGyro = app.sensorMpu9250GyroConvert(a[2]);
	yGyro = app.sensorMpu9250GyroConvert(a[1]);
	xGyro = app.sensorMpu9250GyroConvert(a[0]);
	zMag = a[8];
	yMag = a[7];
	xMag = a[6];

	yawSend = yaw;

	if (this.accelCounter > 20) {
		this.accelCounter = 0;
		publishVals(this.deviceId, this.pitch);
	} else {
		publishOrientationVals(this.deviceId, this.pitch);
		this.accelCounter++;
	}

	if (this.gpsCounter > 600) {
		this.gpsCounter = 0;
		function disp(pos) {
			lat = pos.coords.latitude;
			lon = pos.coords.longitude;
			if (latPrev != lat || lonPrev != lon) {
				latPrev = lat;
				lonPrev = lon;
				publishGps();
			}
		}
		navigator.geolocation.getCurrentPosition(disp);
	} else {
		this.gpsCounter++;
	}
	document.querySelector('#cube .front').style.transform = 'rotateX( ' + (0 + this.pitch) + 'deg ) rotateY( ' + (0 + yaw) + 'deg ) translateZ( 100px )';
	document.querySelector('#cube .back').style.transform = 'rotateX( ' + (180 + this.pitch) + 'deg ) rotateY( ' + (0 - yaw) + 'deg ) translateZ( 100px )';
	document.querySelector('#cube .right').style.transform = 'rotateX( ' + (0 + this.pitch) + 'deg ) rotateY( ' + (90 + yaw) + 'deg ) translateZ( 100px )';
	document.querySelector('#cube .left').style.transform = 'rotateX( ' + (0 + this.pitch) + 'deg ) rotateY( ' + (-90 + yaw) + 'deg ) translateZ( 100px )';
	document.querySelector('#cube .top').style.transform = 'rotateX( ' + (90 + this.pitch) + 'deg ) rotateZ( ' + (0 - yaw) + 'deg ) translateZ( 100px )';
	document.querySelector('#cube .bottom').style.transform = 'rotateX( ' + (-90 + this.pitch) + 'deg ) rotateZ( ' + (0 + yaw) + 'deg ) translateZ( 100px )';


	message = "<b><i class='fa fa-repeat' aria-hidden='true'></i>&nbsp Gyro</b> <br/>" +
    "X: " + app.sensorMpu9250GyroConvert(a[0]) + "<br/>" +
    "Y: " + app.sensorMpu9250GyroConvert(a[1]) + "<br/>" +
    "Z: " + app.sensorMpu9250GyroConvert(a[2]) + "<br/>" +
    "<br/><b><i class='fa fa-arrows-alt' aria-hidden='true'></i>Accel</b> <br/>" +
    "X: " + app.sensorMpu9250AccConvert(a[3]) + "<br/>" +
    "Y: " + app.sensorMpu9250AccConvert(a[4]) + "<br/>" +
    "Z: " + app.sensorMpu9250AccConvert(a[5]) + "<br/>" +
    "<br/><b><i class='fa fa-compass' aria-hidden='true'></i>&nbsp; Mag</b> <br/>" +
    "X: " + a[6] + "<br/>" +
    "Y: " + a[7] + "<br/>" +
    "Z: " + a[8] + "<br/>";

	accelerometerData.innerHTML = message;
};

SensorTag.prototype.onReadRssi = function (data) {
	console.log('read RSSI', data, 'with device', this.deviceId);
};

SensorTag.prototype.onIsConnected = function (data) {
	console.log('(',Date(),')', this.deviceId, 'is Connected (', data, ')');
//	console.log('onIsConnected - isdeviceConnectd: ', this.isdeviceConnectd, ' with device:', this.deviceId);
	if (this.isdeviceConnectd == false)
	{
		this.isdeviceConnectd = true;
		app.setConnection(this.deviceId, data);
	}
	else
	{
		//Read RSSI level
		ble.readRSSI(this.deviceId, this.onReadRssi.bind(this), app.onError);
		//Read Battery level
		ble.read(this.deviceId, battery.service, battery.data, this.onBatteryData.bind(this), app.onError);
	}
};

SensorTag.prototype.onIsDisconnected = function (data) {
	console.log('(', Date(), ')', this.deviceId, 'is Disconnected (', data, ')');
//	console.log('onIsDisconnected - isdeviceConnectd: ', this.isdeviceConnectd, ' with device:', this.deviceId);
	if (this.isdeviceConnectd)
	{
		this.isdeviceConnectd = false;
		unbindThing(this.deviceId);
		this.disconnect();
		app.setConnection(this.deviceId, data);
	}
};

SensorTag.prototype.disconnect = function () {
	clearInterval(this.scheduledInt);
	ble.stopNotification(this.deviceId, button.service, button.data);
	ble.stopNotification(this.deviceId, accelerometer.service, accelerometer.data);
	ble.disconnect(this.deviceId, app.showMainPage(this.deviceId), app.showMainPage(this.deviceId));
/* 	if (this.deviceId == "A0:E6:F8:AE:CA:04")
		app.sendSms("Fire Extinguisher Ext 02 Disconnected");
	else if (this.deviceId == "B0:B4:48:D2:33:06")
		app.sendSms("Fire Extinguisher Ext 01 Disconnected");
 */	if (isReconnectInProgress == false) {
		isReconnectInProgress = true;
		app.reconnect();
		reconnectInt = setInterval(app.reconnect, 10000);
	}
};


/*window.onbeforeunload = function () {
    if(connectedDeviceID != 0){
        ble.disconnect(connectedDeviceID, app.showMainPage, app.onError);
		console.log("Disconnection Error");
    }
};
*/

//document.addEventListener("resume",onResume, false);

/*function onResume() {
	console.log("OnResume");
    location.reload();
    
    if(connectedDeviceID != 0){
        ble.disconnect(connectedDeviceID, app.showMainPage, app.onError);
		console.log("Disconnection Error");
    }
}
*/
var app = {
	sensorTags: [],
	sensorTagsDisconnected: [],
	tempSensor: [],

	initialize: function () {
		initializeUI();
		this.bindEvents();
		$("#detailPage").hide();
	},
	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
		refreshButton.addEventListener('touchstart', this.refreshDeviceList, false);
		disconnectButton.addEventListener('touchstart', this.disconnect, false);
		//deviceList.addEventListener('touchstart', this.connect, false); // assume not scrolling
	},
	onDeviceReady: function () {
		console.log("connectPortal initialize2"); 
		connectPortal();
		window.plugins.insomnia.keepAwake();
		//app.refreshDeviceList();
	},
	refreshDeviceList: function () {
//		$("#deviceList").empty(); GABI
//		deviceList.innerHTML = '';
		showModalRefresh();
		ble.scan(['AA80'], 5, app.onDiscoverDevice, app.onError);
		console.log("BLE Scan");
		searchingForDevices = true;
		setTimeout(function () {
			if (searchingForDevices == true) {
				hideModal();
			}
		}, 5000);
	},
	onDiscoverDevice: function (device) {
		var deviceName;

		hideModal();
		searchingForDevices = false;
		if (device.id == "A0:E6:F8:AE:CA:04")
			deviceName = "Ext 02";
		else if (device.id == "B0:B4:48:D2:33:06")
			deviceName = "Ext 01";

		listItem = document.createElement('li'),
		html = device.name + '&nbsp;|&nbsp;' + deviceName + '&nbsp;|&nbsp;' + device.rssi + '<br/>Connect';
		listItem.dataset.deviceId = device.id;  // TODO
		listItem.innerHTML = html;
		listItem.onclick = function () {
			app.connect(device.id);
		};
		deviceList.appendChild(listItem);
		console.log("sensor: " + listItem.dataset.deviceId);
//		if ((device.id == 'B0:B4:48:D2:33:06') || (device.id == 'A0:E6:F8:AE:CA:04'))
//			setTimeout(app.connect(device.id), 1000);
	},
	connect: function (id) {
		var isExists = false;

		$("#modal").html("Connecting...");
		$("#modalPage").show();

		app.sensorTags.forEach(function (listItem, i) {
			console.log("sensorTag[" + i + "] = " + app.sensorTags[i].deviceId);
			if (app.sensorTags[i].deviceId == id)
				isExists = true;
		});

		if (isExists == false) {
			var deviceId = id,

			sensorTag = new SensorTag(deviceId);  // TODO store name too
			console.log("sensorTag = " + sensorTag.deviceId);
			app.sensorTags.push(sensorTag);
			sensorTag.connect();
		}
	},

	sensorMpu9250GyroConvert: function (data) {
		return data / (65536 / 500);
	},

	sensorMpu9250AccConvert: function (data) {
		// Change  /2 to match accel range...i.e. 16 g would be /16
		return data / (32768 / 2);
	},

	onLuxometerData: function (data) {
		var value = new Uint16Array(data);
		var mantissa = value[0] & 0x0FFF
		var exponent = value[0] >> 12

		magnitude = Math.pow(2, exponent)
		output = (mantissa * magnitude)

		lux = output / 100;
		if (button1val != 1 && button2val != 1) {
			var lux_norm = lux / 100 + .5;
			lux_norm = lux_norm * -1 + 1;
			var colx = 0;
			var coly = 0;
			var finalcolor = "hsla(" + colx + ", " + coly + "%, " + "0%, " + lux_norm + ")";
			$('#cube figure').css('background', finalcolor);
		}
	},
/*	disconnect: function (event) {
		var deviceId = event.target.dataset.deviceId;
		ble.disconnect(deviceId, app.showMainPage, app.onError);
		console.log("write disconnect2 Error");
	},*/
	showMainPage: function (devId) {
//		listItem.dataset.deviceId == devId ?
		connectedDeviceID = 0;
		$("#detailPage").hide();
		$("#mainPage").show();
	},
	showDetailPage: function () {
		hideModal();
		$("#mainPage").hide();
		$("#detailPage").show();
	},
	onError: function (reason) {
		searchingForDevices = false;
		console.log("onError: " + reason);
		/*     $("#deviceList").empty();		GABI
		 */    hideModal();
	},
	setConnection: function (id, state) {
		var element = document.getElementsByTagName('li');
		if (state == 'OK')
			for (var i = 0; i < element.length; i++) {
				element[i].dataset.deviceId == id ? element[i].style.background = 'green' : 0;
				hideModal();
			}
		else
			for (var i = 0; i < element.length; i++) {
				element[i].dataset.deviceId == id ? element[i].style.background = 'gray' : 0;

/*				GABI 11.1.17
				// Remove from UI deviceList
				if (element[i].dataset.deviceId == id) {
					deviceList.removeChild(element[i]);
				}
*/

				// Remove from sensors array
				if (app.sensorTags[i].deviceId == id) {
//					console.log("DISCONNECTING");
//					console.log("app.sensorTags[i] = " + app.sensorTags[i].deviceId);
					app.sensorTagsDisconnected.push(app.sensorTags[i]);
					app.sensorTags.splice(i, 1);
//					console.log("app.sensorTagsDisconnected[i] = " + app.sensorTagsDisconnected[i].deviceId);
//					console.log("app.sensorTags[i] = " + app.sensorTags[i].deviceId);
				}

			}
	},

	reconnect: function () {
		var isExists = false;
		console.log("RECONNECTING ");

		ble.scan(['AA80'], 5, function (device) {
			app.tempSensor.forEach(function (listItem, i) {
				if (listItem == device.id)
					isExists = true;
				console.log("sensorTag[" + i + "] = " + app.tempSensor[i]);
			});
			if(!isExists)
				app.tempSensor.push(device.id);
		}, app.onError);

		setTimeout(function () {
			app.sensorTagsDisconnected.forEach(function (listItem, i) {
				app.tempSensor.forEach(function (listItem2, j) {
					console.log("sensorTagsDisconnected.length " + app.sensorTagsDisconnected.length);
					console.log("sensorTagsDisconnected[" + i + "] = " + listItem.deviceId);
					console.log("devId = " + listItem2);
					if (listItem.deviceId == listItem2) {
						sensorTag = new SensorTag(listItem.deviceId);
//						console.log("sensorTag = " + sensorTag.deviceId);
//						console.log("app.sensorTagsDisconnected[i] = " + listItem.deviceId);
						app.sensorTags.push(sensorTag);
//						console.log("RECONNECTING0000");
						app.tempSensor.splice(j, 1);
						app.sensorTagsDisconnected.splice(i, 1);
//						console.log("app.sensorTags[i] = " + app.sensorTags[i].deviceId);
//						console.log("RECONNECTING11111");
//						console.log("RECONNECTING22222");
						sensorTag.connect();
/* 						if (listItem2 == "A0:E6:F8:AE:CA:04")
							app.sendSms("Fire Extinguisher Ext 02 Reconnected");
						else if (listItem2 == "B0:B4:48:D2:33:06")
							app.sendSms("Fire Extinguisher Ext 01 Reconnected");
 */					}
				});
			});
			console.log("sensorTagsDisconnected.length " + app.sensorTagsDisconnected.length);
			if (app.sensorTagsDisconnected.length == 0) {
				clearInterval(reconnectInt);
				isReconnectInProgress = false;
			}

		},5000);
	},
	
	sendSms: function(message) {
		var number = '00972537333801';
		console.log("number=" + number + ", message= " + message);

			//CONFIGURATION
		var options = {
			replaceLineBreaks: false, // true to replace \n by a new line, false by default
			android: {
				//intent: 'INTENT'  // send SMS with the native android SMS messaging
				intent: '' // send SMS without open any other app
			}
		};

		var success = function () { console.log('Message sent successfully'); };
		var error = function (e) { console.log('Message Failed:' + e); };
		sms.send(number, message, options, success, error);
	}
};
