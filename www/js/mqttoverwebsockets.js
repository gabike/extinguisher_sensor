var zAccel =0, yAccel =0, xAccel =0, button1val =0, button2val=0, lux=0, yaw=0, yawSend=0, pitchSend=0;
var lat=0, lon=0, xMag=0, yMag=0, zMag=0, xGyro=0, yGyro=0, zGyro=0, temperature=0, pressure=0;

var zAccelPrev = 0, yAccelPrev = 0, xAccelPrev = 0, button1valPrev = 0, button2valPrev = 0, luxPrev = 0, yawPrev = 0, pitchPrev = 0;
var latPrev=0, lonPrev=0, xMagPrev=0, yMagPrev=0, zMagPrev=0, xGyroPrev=0, yGyroPrev=0, zGyroPrev=0, temperaturePrev=0, pressurePrev=0;

var connected=0;

$(function() {
	if ($('#url').val() == '')
		$('#url').val(
				window.location.protocol + '//' + window.location.hostname
						+ ':8443/api');
});

var portraitScreenHeight;
var landscapeScreenHeight;
var initializeUI = function(){
	if(window.orientation === 0 || window.orientation === 180){
		portraitScreenHeight = $(window).height();
		landscapeScreenHeight = $(window).width();
	}
	else{
		portraitScreenHeight = $(window).width();
		landscapeScreenHeight = $(window).height();
	}

	var tolerance = 25;
	$(window).bind('resize', function(){
		if((window.orientation === 0 || window.orientation === 180) &&
		   ((window.innerHeight + tolerance) < portraitScreenHeight)){
			// keyboard visible in portrait
			$("#signature").hide();
		}
		else if((window.innerHeight + tolerance) < landscapeScreenHeight){
			// keyboard visible in landscape
			$("#signature").hide();
		}
		else{
			$("#signature").show();
		}
	});
}
var showModalRefresh = function(){
	$("#modal").html("Scanning for tags...");
	$("#modalPage").show();
}
var hideModal = function(){
	$("#modalPage").hide();
}
var publishGps = function(){
	var thingkey = "ti-"+device.uuid;
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var req = 
	'{'+
		'"publishGps" : {' +
			'"command" : "location.publish",'+
			'"params" : {'+
				'"thingKey" : "'+thingkey+'",'+
				'"lat" : '+lat+','+
				'"lng" : '+lon+','+
				'"fixType" : "gps",'+
				'"fixAcc" : 3'+
			'}'+
		'}'+
	'}';

	try {
		JSON.parse(req);
	} catch (e) {
		alert('Invalid JSON!' + e)
		return;
	}

	message = new Paho.MQTT.Message(req);
	
	message.destinationName = "api";
	client.send(message);
}
var createThing = function(dev){
	var thingkey = "ti-"+dev.replace(/:/g, "-");
	var lastFive = thingkey.substr(thingkey.length - 2); 
	var name = "TI Sensor Tag - "+lastFive;
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var req = 
	'{'+
		'"createThing": {'+
			'"command": "thing.create",'+
			'"params": {'+
				'"name": "'+name+'",'+
				'"key": "'+thingkey+'",'+      
				'"attrs": {'+  
					'"name": {'+  
					  '"value": "Name not set"'+  
					'}'+  
				  '},'+  
				'"defKey": "tisensortag",' +
				'"desc": "",'+
				'"tags": ["tisensortag"],'+
				'"iccid": "",'+
				'"esn": ""'+
			'}'+
		'}'+
	'}';

	try {
		JSON.parse(req);
	} catch (e) {
		alert('Invalid JSON!' + e)
		return;
	}

	message = new Paho.MQTT.Message(req);
	
	message.destinationName = "api";
	client.send(message);
}

var bindThing = function (dev) {
	var thingkey = "ti-" + dev.replace(/:/g, "-");
	var lastFive = thingkey.substr(thingkey.length - 5);
	var name = "Ext Sensor Tag - " + lastFive;

	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var req =
	'{' +
		'"bindThing": {' +
			'"command": "thing.bind",' +
			'"params": {' +
				'"key": "' + thingkey + '",' +
				'"autoDefKey": "tisensortag",' +
				'"autoDefThingName": "' + name + '",' +
				'"autoDefTags": "fire_ext"' +
			'}' +
		'}' +
	'}';

	try {
		JSON.parse(req);
	} catch (e) {
		alert('Invalid JSON!' + e)
		return;
	}

	message = new Paho.MQTT.Message(req);

	message.destinationName = "api";
	client.send(message);
}

var unbindThing = function (dev) {
	var thingkey = "ti-" + dev.replace(/:/g, "-");

	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var req =
	'{' +
		'"unbindThing": {' +
			'"command": "thing.unbind",' +
			'"params": {' +
				'"key": "' + thingkey + '"' +
			'}' +
		'}' +
	'}';

	try {
		JSON.parse(req);
	} catch (e) {
		alert('Invalid JSON!' + e)
		return;
	}

	message = new Paho.MQTT.Message(req);

	message.destinationName = "api";
	client.send(message);
}
var addBindedName = function(dev){
	var thingkey = "ti-" + dev.replace(/:/g, "-");
	var lastFive = thingkey.substr(thingkey.length - 5);
	var name = "Ext Sensor Tag - " + lastFive;

	if (connected == false) {
		alert('Connect first!');
		return;
	}

  	var req2 =
	'{' +
		'"bindName": {' +
			'"command" : "attribute.publish",' +
			'"params" : {' +
				'"thingKey": "' + thingkey + '",' +
				'"key": "name",' +
				'"value":"' + name + '"' +
			'}' +
		'}' +
	'}';

  	if (req2 == '') {
  		alert('Empty request!');
  		return;
  	}

  	try {
  		JSON.parse(req2);
  	} catch (e) {
  		console.log("Error" + req2);
  		return;
  	}

  	message = new Paho.MQTT.Message(req2);

  	message.destinationName = "api";
  	client.send(message);
}

var publishButton = function (dev) {
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var thingkey = "ti-" + dev.replace(/:/g, "-");
	var newVal = false;
	var req =
	'{' +
		'"publishButton": {' +
			'"command": "property.batch",' +
			'"params": {' +
				'"thingKey": "' + thingkey + '",' +
				'"key": "batchkey",' +
				'"aggregate":false,' +
				'"data": [';

	if (button1val != button1valPrev) {
		newVal = true;
		button1valPrev = button1val;
		req += '{' +
			'"key": "button1",' +
			'"value": ' + button1val +
		'},';
	}
	if (button2val != button2valPrev) {
		newVal = true;
		button2valPrev = button2val;
		req += '{' +
			'"key": "button2",' +
			'"value": ' + button2val +
		'},';
	}

	req = req.substring(0, req.length - 1);
	req += ']}}}';

	if (newVal) {
		if (req == '') {
			alert('Empty request!');
			return;
		}

		try {
			JSON.parse(req);
		} catch (e) {
			alert('Invalid JSON!' + e)
			return;
		}

		message = new Paho.MQTT.Message(req);

		message.destinationName = "api";
		client.send(message);
	}

}

var publishBattery = function (dev, battLevel) {
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var thingkey = "ti-" + dev.replace(/:/g, "-");
	var newVal = false;
	var req =
	'{' +
		'"publishBattery": {' +
			'"command": "property.batch",' +
			'"params": {' +
				'"thingKey": "' + thingkey + '",' +
				'"key": "batchkey",' +
				'"aggregate":false,' +
				'"data": [';

	newVal = true;
	req += '{' +
		'"key": "batt",' +
		'"value": ' + battLevel +
	'},';

	req = req.substring(0, req.length - 1);
	req += ']}}}';

	if (newVal) {
		if (req == '') {
			alert('Empty request!');
			return;
		}

		try {
			JSON.parse(req);
		} catch (e) {
			alert('Invalid JSON!' + e)
			return;
		}

		message = new Paho.MQTT.Message(req);

		message.destinationName = "api";
		client.send(message);
	}

}

var publishOrientationVals = function (dev, pitch) {
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var thingkey = "ti-" + dev.replace(/:/g, "-");
	var newVal = false;
	var req = 
	'{'+
		'"1": {'+
			'"command": "property.batch",'+
			'"params": {'+
				'"thingKey": "'+thingkey+'",'+
				'"key": "batchkey",'+
				'"aggregate":false,'+
				'"data": [';
				
/*	if(Math.abs(yawPrev-yawSend)>1.0){
		newVal = true;
		yawPrev=yawSend;
		req +=	'{'+
					'"key": "yaw",'+
					'"value": '+yawSend+
				'},';
	}
*/
	if (Math.abs(pitchPrev - pitch) > 1.0) {
		newVal = true;
		pitchPrev = pitch;
		req +=	'{'+
			'"key": "pitch",'+
			'"value": ' + pitch +
		'},';
	}

	if(Math.abs(luxPrev-lux)>20){
		newVal = true;
		luxPrev=lux;
		req +=	'{'+
			'"key": "luxometer",'+
			'"value": '+lux+
		'},';
	}
/*
	if (batteryLevel != batteryLevelPrev) {
		newVal = true;
		batteryLevelPrev = batteryLevel;
		req += '{' +
			'"key": "batt",' +
			'"value": ' + batteryLevel +
		'},';
	}
*/
	req = req.substring(0,req.length-1);
	req +=	']}}}';	

	if(newVal){
		if (req == '') {
			alert('Empty request!');
			return;
		}

		try {
			JSON.parse(req);
		} catch (e) {
			alert('Invalid JSON!' + e)
			return;
		}	

		message = new Paho.MQTT.Message(req);
		
		message.destinationName = "api";
		client.send(message);
	}
}

var publishVals = function(dev, pitch) {
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var thingkey = "ti-" + dev.replace(/:/g, "-");
	var newVal = false;
	var req = 
	'{'+
		'"1": {'+
			'"command": "property.batch",'+
			'"params": {'+
				'"thingKey": "'+thingkey+'",'+
				'"key": "batchkey",'+
				'"aggregate":false,'+
				'"data": [';

/*	if(Math.abs(yawPrev-yawSend)>1.0){
		newVal = true;
		yawPrev=yawSend;
		req +=	'{'+
					'"key": "yaw",'+
					'"value": '+yawSend+
				'},';
	}
*/
	if (Math.abs(pitchPrev - pitch) > 1.0) {
		newVal = true;
		pitchPrev = pitch;
		req +=	'{'+
			'"key": "pitch",'+
			'"value": ' + pitch +
		'},';
	}
	if(Math.abs(zAccelPrev-zAccel)>0.1){
		newVal = true;
		zAccelPrev=zAccel;
		req +=	'{'+
			'"key": "z_Accel",'+
			'"value": '+zAccel+
		'},';
	}
	if(Math.abs(yAccelPrev-yAccel)>0.1){
		newVal = true;
		yAccelPrev=yAccel;
		req +=	'{'+
			'"key": "y_Accel",'+
			'"value": '+yAccel+
		'},';
	}
	if(Math.abs(xAccelPrev-xAccel)>0.1){
		newVal = true;
		xAccelPrev=xAccel;
		req +=	'{'+
			'"key": "x_Accel",'+
			'"value": '+xAccel+
		'},';
	}
	if(Math.abs(zGyroPrev-zGyro)>5.0){
		newVal = true;
		zGyroPrev=zGyro;
		req +=	'{'+
			'"key": "z_Gyro",'+
			'"value": '+zGyro+
		'},';
	}
	if(Math.abs(yGyroPrev-yGyro)>5.0){
		newVal = true;
		yGyroPrev=yGyro;
		req +=	'{'+
			'"key": "y_Gyro",'+
			'"value": '+yGyro+
		'},';
	}
	if(Math.abs(xGyroPrev-xGyro)>5.0){
		newVal = true;
		xGyroPrev=xGyro;
		req +=	'{'+
			'"key": "x_Gyro",'+
			'"value": '+xGyro+
		'},';
	}
	if(Math.abs(zMagPrev-zMag)>30.0){
		newVal = true;
		zMagPrev=zMag;
		req +=	'{'+
			'"key": "z_Mag",'+
			'"value": '+zMag+
		'},';
	}
	if(Math.abs(yMagPrev-yMag)>30.0){
		newVal = true;
		yMagPrev=yMag;
		req +=	'{'+
			'"key": "y_Mag",'+
			'"value": '+yMag+
		'},';
	}
	if(Math.abs(xMagPrev-xMag)>30.0){
		newVal = true;
		xMagPrev=xMag;
		req +=	'{'+
			'"key": "x_Mag",'+
			'"value": '+xMag+
		'},';
	}
	if(Math.abs(temperaturePrev-temperature)>0.01){
		newVal = true;
		temperaturePrev=temperature;
		req +=	'{'+
			'"key": "temperature",'+
			'"value": '+temperature+
		'},';
	}
	if(Math.abs(pressurePrev-pressure)>0.02){
		newVal = true;
		pressurePrev=pressure;
		req +=	'{'+
			'"key": "pressure",'+
			'"value": '+pressure+
		'},';
	}
	if(Math.abs(luxPrev-lux)>20){
		newVal = true;
		luxPrev=lux;
		req +=	'{'+
			'"key": "luxometer",'+
			'"value": '+lux+
		'},';
	}

/*
	if (batteryLevel != batteryLevelPrev) {
		newVal = true;
		batteryLevelPrev = batteryLevel;
		req += '{' +
			'"key": "batt",' +
			'"value": ' + batteryLevel +
		'},';
	}
*/
	req = req.substring(0,req.length-1);
	req +=	']}}}';

	if(newVal){
		if (req == '') {
			alert('Empty request!');
			return;
		}

		try {
			JSON.parse(req);
		} catch (e) {
			alert('Invalid JSON!' + e)
			return;
		}	

		message = new Paho.MQTT.Message(req);
		
		message.destinationName = "api";
		client.send(message);
	}
}
var gotResults = function(msg) {
	try {
		msg = JSON.stringify(JSON.parse(msg), undefined, 4);
	} catch (e) {
		alert('Invalid JSON!' + e)
	}
}

var connected = false;

var subscribe = function() {

	if (connected == false) {
		alert('Connect first!');
		return;
	}

	var topic = "thing/#sensortag1";

	client.subscribe(topic);

	//$('#subscribe-label').html('Topic: <b>' + topic + '</b>');

	//$('#subscribe-box').html('');
}

var publishName = function(){
	console.log("Update name");
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	$("#modal").html("Updating name...");
	$("#modalPage").show();
	setTimeout(
	  function() 
	  {
		var thingkey = "ti-"+device.uuid;
		var req = 
		'{'+
			'"1": {'+
				'"command" : "attribute.publish",'+
				'"params" : {'+
					'"thingKey": "'+thingkey+'",'+
					'"key": "name",'+
					'"value":"'+$("#name").val()+'"'+
				'}'+
			'}'+
		'}';
		
		if (req == '') {
			alert('Empty request!');
			return;
		}

		try {
			JSON.parse(req);
		} catch (e) {
			$("#modalPage").hide();
			return;
		}
		
		message = new Paho.MQTT.Message(req);
		
		message.destinationName = "api";
		client.send(message);
	}, 2000);
	
}

var getName = function() {
	if (connected == false) {
		alert('Connect first!');
		return;
	}
	var thingkey = "ti-"+device.uuid;
	var req = 
	'{'+
		'"getName": {'+
			'"command" : "attribute.current",'+
			'"params" : {'+
				'"thingKey": "'+thingkey+'",'+
				'"key": "name"'+
			'}'+
		'}'+
	'}';
	
	

	if (req == '') {
		alert('Empty request!');
		return;
	}

	try {
		JSON.parse(req);
	} catch (e) {
		alert('Invalid JSON!' + e)
		return;
	}
	
	message = new Paho.MQTT.Message(req);
	
	message.destinationName = "api";
	client.send(message);
}

var connectPortal = function() {

	var host = "api-dev.devicewise.com";

	var port = 443;

	var ssl = port != 80 && port != 8080

	
	var thingkey = "ti-"+device.uuid;
	console.log(thingkey);
	
	var clientId = thingkey;
	var username = thingkey;
	var password = "demo_ti_ble_ext";
	
	if(connected==1){
		return;
	}
	
	console.log("connectPortal");
	client = new Paho.MQTT.Client(host, port, '/mqtt' + (ssl ? '-ssl' : ''),
			clientId);

	client.onConnectionLost = onConnectionLost;
	client.onMessageArrived = onMessageArrived;
	
	client.connect({
		userName : username,
		password : password,
		useSSL : ssl,
		mqttVersion : 3,
		onSuccess : onConnect,
		onFailure : onFailure
	});

	function onFailure(responseObject) {
		console.log("failed");
		alert('Failed to connect: ' + responseObject.errorMessage);
	}

	function onConnect() {
		connected=1;
		console.log("connected");
		//createThing();
		getName();
    
		function disp(pos) {
			lat = pos.coords.latitude;
			lon = pos.coords.longitude;
		}
		navigator.geolocation.getCurrentPosition(disp);
		
		client.subscribe("reply");
	}

	function onConnectionLost(responseObject) {

	}

	function onMessageArrived(message) {
		var obj = jQuery.parseJSON(message.payloadString);
		$("#modalPage").hide();
		if(obj['getName']){			
			if(obj['getName'].errorCodes){
				 return;
			}
			var name = obj['getName'].params.value;
			$("#name").val(name);
		}
	}
}
var littleEndianToUint8ZeroethIndex = function(data)
{
	return data[0];
}
var littleEndianToUint8FirstIndex = function(data)
{
	return data[1];
}
var littleEndianToUint16 = function(data)
{
	return (littleEndianToUint8FirstIndex(data) << 8) +
		littleEndianToUint8ZeroethIndex(data);
}
